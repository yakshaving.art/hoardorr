#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# dive.sh: compile dive for our specific architecures and store those as artifacts
# Usage: bash path/to/dive.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='dive'
UTILITY_GITHUB='wagoodman/dive'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${DIVE_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# we build them because we need arm64
git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
# shellcheck disable=SC2064 # cleanup, yes, expand now
trap "rm -rf $(pwd)/build" EXIT
cd build || exit
git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"
	CGO_ENABLED=0 GOOS="${os}" GOARCH="${arch}" go build -a -ldflags '-extldflags "-static"' -o "${UTILITY_NAME}-${os}-${arch}"

	_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"

	# add shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"

	# move to proper artifact location for CI to pick them up
	mv "${UTILITY_NAME}-${os}-${arch}" "${UTILITY_NAME}-${os}-${arch}.sha256" ../
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
