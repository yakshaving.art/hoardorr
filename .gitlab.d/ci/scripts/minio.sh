#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# minio.sh: compile minio for our specific architecures and store those as artifacts
# Usage: bash path/to/minio.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='minio'
UTILITY_GITHUB='minio/minio'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${MINIO_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# now that upstream has linux/arm64 too, we just download it
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	export TARGET_BIN="${UTILITY_NAME}-${os}-${arch}"

	# fetch upstream and its sha256 and check it
	curl -qsSLO "https://dl.min.io/server/minio/release/${os}-${arch}/minio.${UTILITY_VERSION}{,.sha256sum}"
	sha256sum -c < "minio.${UTILITY_VERSION}.sha256sum"
	rm -f "minio.${UTILITY_VERSION}.sha256sum"

	# try stripping
	mv "minio.${UTILITY_VERSION}" "${TARGET_BIN}"
	_multiarch_strip "${TARGET_BIN}" "${arch}"

	# add shasums after stripping
	sha256sum "${TARGET_BIN}" >| "${TARGET_BIN}.sha256"
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
