#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# dasel.sh: compile dasel for our specific architecures and store those as artifacts
# Usage: bash path/to/dasel.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='dasel'
UTILITY_GITHUB='TomWright/dasel'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${DASEL_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# we build them because we need arm64
git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
# shellcheck disable=SC2064 # cleanup, yes, expand now
trap "rm -rf $(pwd)/build" EXIT
cd build || exit
git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"
	bin="${UTILITY_NAME}-${os}-${arch}"
	CGO_ENABLED=0 GOOS="${os}" GOARCH="${arch}" go build -o "${bin}" -ldflags="-X 'github.com/${UTILITY_GITHUB}/internal.Version=${UTILITY_VERSION}' -extldflags '-static'" ./cmd/dasel/

	# strip it
	_multiarch_strip "${bin}" "${arch}"

	# add shasums
	sha256sum "${bin}" >| "${bin}.sha256"

	# move to proper artifact location for CI to pick them up
	mv "${bin}" "${bin}.sha256" ../
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
