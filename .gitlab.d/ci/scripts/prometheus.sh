#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# prometheus.sh: install prometheus tools
# Usage: bash path/to/prometheus.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

while read -r; do
	>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}'"; exit 42; }
done <<-EOF
	parallel
	sha256sum
	wget
EOF


# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

function getit() {
	local tool="${1:-unset}"
	local version="${2:-unset}"

	if [[ "unset" == "${tool}" || "unset" == "${version}" || "$#" != 2 ]]; then
		echo "usage: getit tool version"
		exit 42
	fi

	# determine latest version if it is explicitly set to "latest"
	if [[ "latest" == "${version}" ]]; then
		version="$(_get_latest_version_from_github "prometheus/${tool}")"
		version="${version#v}"

		_skip_if_already_latest "${tool}" "${version}"
	fi

	tmp="$(mktemp -d)"

	local gh_url="https://github.com/prometheus/${tool}/releases/download/v${version}/"

	# fetch shasums
	if ! wget -qqP "${tmp}" "${gh_url}/sha256sums.txt"; then
		>&2 echo "error: can't download shasums files from ${gh_url}!"
		return 255
	fi

	# process binaries for each arch
	IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
	for os_arch in "${MUCHOS_ARCHES[@]}"; do
		os="${os_arch%%/*}"
		arch="${os_arch##*/}"

		# fetch all required files into tmp dir
		if ! wget -qqP "${tmp}" "${gh_url}/${tool}-${version}.${os}-${arch}.tar.gz"; then
			>&2 echo "error: can't download files from ${gh_url}!"
			return 255
		fi

		# check shasums (sed to normalize paths)
		if ! grep ".${os}-${arch}.tar.gz$" "${tmp}/sha256sums.txt" | sed "s|  |  ${tmp}/|" | sha256sum -c >/dev/null; then
			>&2 echo "error: can't verify shasums"
			return 255
		fi

		# unpack (and try to strip)
		tar xf "${tmp}/${tool}-${version}.${os}-${arch}.tar.gz" \
			"${tool}-${version}.${os}-${arch}/${tool}" \
			--strip-components=1

		mv "${tool}" "${tool}-${os}-${arch}"
		_multiarch_strip "${tool}-${os}-${arch}" "${arch}"

		# generate hoardorr shasums
		sha256sum "${tool}-${os}-${arch}" >| "${tool}-${os}-${arch}.sha256"

		echo "	Hoarded prometheus's '${tool}' tool, version '${version}', for os/arch '${os}/${arch}'"
	done

	# cleanup
	rm -rf "${tmp}"

	# store installed version separately
	VERSION_FILE="${tool}-version-hoarded"
	echo -n "${version}" >| "${VERSION_FILE}"
}
export -f getit

# hoard all the tools with version defined
env \
	| gawk -F '[_=]' '/^PROMETHEUS_.*_VERSION=/ { cmd=gensub(/PROMETHEUS_(.*)_VERSION=(.*)/, "getit \\1 \\2", "g"); print tolower(cmd) }' \
	| sort \
	| SHELL="/bin/bash" parallel --no-notice
