#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This is a wrapper around trivy invocation
set -EeufCo pipefail
IFS=$'\t\n'

# trivy defaults
export TRIVY_QUIET="${TRIVY_QUIET:-true}"

TRIVY_BIN="$(command -v trivy || true)"		# NOTE: mask exit code
TRIVY_BIN="${TRIVY_BIN:-/usr/bin/trivy}"	# so that this line handles it

# TODO: proper arch detection depending on a runner
cp ./trivy-linux-amd64 "${TRIVY_BIN}"
chmod a+x "${TRIVY_BIN}"

if [[ "true" == "${GITLAB_CI:-unset}" ]]; then
	# in case we decide to cache it on CI later
	export TRIVY_CACHE_DIR="${CI_PROJECT_DIR:-.}/.cache/trivy"
	export TRIVY_SKIP_DIRS="${CI_PROJECT_DIR:-.}/.cache"

	# output all the issues first
	"${TRIVY_BIN}" config --exit-code 0 .
	"${TRIVY_BIN}" rootfs --exit-code 0 .

	# fail pipeline on critical and high issues
	"${TRIVY_BIN}" config --exit-code 42 --severity CRITICAL,HIGH .
	"${TRIVY_BIN}" rootfs --exit-code 43 --severity CRITICAL,HIGH .
else
	# locally, we only care about simply running the checks
	"${TRIVY_BIN}" config .
	"${TRIVY_BIN}" rootfs .
fi
