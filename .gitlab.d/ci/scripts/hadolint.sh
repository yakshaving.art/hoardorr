#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# hadolint.sh: compile hadolint for our specific architecures and store those as artifacts
# Usage: bash path/to/hadolint.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='hadolint'
UTILITY_GITHUB='hadolint/hadolint'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${HADOLINT_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# hoard for all the architectures
# this part is tooling specific

# hoard for all the enabled architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	# hadolint's naming is different from ours
	case "${os}" in
		linux)
			hadolint_os='Linux'
			;;
		*)
			echo 'this was not needed yet, so please add. exiting'
			exit 42
			;;
	esac
	case "${arch}" in
		amd64)
			hadolint_arch='x86_64'
			;;
		arm64)
			hadolint_arch='arm64'
			;;
		*)
			echo 'this was not needed yet, so please add. exiting'
			exit 42
			;;
	esac

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/hadolint-${hadolint_os}-${hadolint_arch}" \
		> "hadolint-${hadolint_os}-${hadolint_arch}"

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/hadolint-${hadolint_os}-${hadolint_arch}.sha256" \
		| sha256sum -c

	mv "hadolint-${hadolint_os}-${hadolint_arch}" "${UTILITY_NAME}-${os}-${arch}"

	# try stripping
	_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"	|| true	# hadolint has no sections

	# add our own shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
