#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# goreleaser.sh: compile goreleaser for our specific architecures and store those as artifacts
# Usage: bash path/to/goreleaser.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='goreleaser'
UTILITY_GITHUB='goreleaser/goreleaser'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${GORELEASER_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"


# some building blocks (new WIP)
# shellcheck disable=SC2120	# for now, no idea what the interface would be
function _fetch() {
	# fetches from GH or whatever,
	# leaves binary with a specific name as an artifact,
	# conforming to our naming scheme
	# as in: some-tool-linux-arm64

	# shadow global var for now, until I stabilize this
	local UTILITY_NAME="${1:-${UTILITY_NAME}}"
	local UTILITY_GITHUB="${2:-${UTILITY_GITHUB}}"
	local UTILITY_VERSION="${3:-${UTILITY_VERSION}}"

	# first, download and verify checksums.txt, as per
	# https://goreleaser.com/install/#verifying-the-artifacts
	curl -qsSLO "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/checksums.txt"
	cosign verify-blob \
		--certificate-identity "https://github.com/${UTILITY_GITHUB}/.github/workflows/release.yml@refs/tags/${UTILITY_VERSION}" \
		--certificate-oidc-issuer "https://token.actions.githubusercontent.com" \
		--cert "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/checksums.txt.pem" \
		--signature "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/checksums.txt.sig" \
		./checksums.txt

	# then, process infividual arches
	# shellcheck disable=SC2128
	IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
	for os_arch in "${MUCHOS_ARCHES[@]}"; do
		os="${os_arch%%/*}"
		arch="${os_arch##*/}"
		# here's the ugly part of mapping. Explicitly fail un new arches.
		case "${os}-${arch}" in
			"linux-amd64")
				curl -qsSLO "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/goreleaser_Linux_x86_64.tar.gz"
				sha256sum \
					--check \
					--strict \
					--ignore-missing \
					checksums.txt
				# now it is safe to extract
				tar zxvf goreleaser_Linux_x86_64.tar.gz goreleaser
				mv goreleaser "${UTILITY_NAME}-${os}-${arch}"
				rm -f goreleaser_Linux_x86_64.tar.gz
				;;
			"linux-arm64")
				curl -qsSLO "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/goreleaser_Linux_arm64.tar.gz"
				sha256sum \
					--check \
					--strict \
					--ignore-missing \
					checksums.txt
				# now it is safe to extract
				tar zxvf goreleaser_Linux_arm64.tar.gz goreleaser
				mv goreleaser "${UTILITY_NAME}-${os}-${arch}"
				rm -f goreleaser_Linux_arm64.tar.gz
				;;
			*)
				>&2 echo "Unknown arch ${os_arch}, update the mapping!"
				exit 42
				;;
		esac
	done

	# cleanup
	rm -f checksums.txt
}

function _build() {
	# kept in case we need to rebuild things again
	git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
	# shellcheck disable=SC2064 # cleanup, yes, expand now
	trap "rm -rf $(pwd)/build" EXIT
	cd build || exit
	git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

	# build for all the architectures
	# shellcheck disable=SC2128
	IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
	for os_arch in "${MUCHOS_ARCHES[@]}"; do
		os="${os_arch%%/*}"
		arch="${os_arch##*/}"
		CGO_ENABLED=0 GOOS="${os}" GOARCH="${arch}" go build -a -ldflags '-extldflags "-static"' -o ../"${UTILITY_NAME}-${os}-${arch}"
	done
}

function _strip_and_rehash() {
	# strips binaries, assuming their existance
	# shellcheck disable=SC2128
	for os_arch in "${MUCHOS_ARCHES[@]}"; do
		os="${os_arch%%/*}"
		arch="${os_arch##*/}"

		_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"
		sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"
	done
}


# shellcheck disable=SC2119
_fetch	# or _build here
_strip_and_rehash

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
