#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# trivy.sh: compile trivy for our specific architecures and store those as artifacts
# Usage: bash path/to/trivy.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='trivy'
UTILITY_GITHUB='aquasecurity/trivy'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${TRIVY_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# hoard for all the architectures
# this part is tooling specific

# fetch checksums first
curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/trivy_${UTILITY_VERSION##v}_checksums.txt" > sums.txt

# hoard for all the enabled architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	# trivy's naming is different from ours
	case "${os}" in
		linux)
			trivy_os='Linux'
			;;
		*)
			echo 'this was not needed yet, so please add. exiting'
			exit 42
			;;
	esac
	case "${arch}" in
		amd64)
			trivy_arch='64bit'
			;;
		arm64)
			trivy_arch='ARM64'
			;;
		*)
			echo 'this was not needed yet, so please add. exiting'
			exit 42
			;;
	esac

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/trivy_${UTILITY_VERSION##v}_${trivy_os}-${trivy_arch}.tar.gz" \
		> "trivy_${UTILITY_VERSION##v}_${trivy_os}-${trivy_arch}.tar.gz"

	grep "${trivy_os}-${trivy_arch}.tar.gz" sums.txt \
		| sha256sum -c

	tar zxf "trivy_${UTILITY_VERSION##v}_${trivy_os}-${trivy_arch}.tar.gz" \
		"trivy" \
		--transform="s/$/-${os}-${arch}/" \
		&& rm -rf "trivy_${UTILITY_VERSION##v}_${trivy_os}-${trivy_arch}.tar.gz"

	# try stripping
	_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"

	# add our own shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"

rm -f sums.txt
