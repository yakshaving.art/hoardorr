#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# hugo.sh: compile hugo for our specific architecures and store those as artifacts
# Usage: bash path/to/hugo.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='hugo'
UTILITY_GITHUB='gohugoio/hugo'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${HUGO_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
#
# fetch checksums first
curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/hugo_${UTILITY_VERSION##v}_checksums.txt" > sums.txt

# hoard for all the enabled architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/hugo_${UTILITY_VERSION##v}_${os}-${arch}.tar.gz" \
		> "hugo_${UTILITY_VERSION##v}_${os}-${arch}.tar.gz"

	grep "hugo_${UTILITY_VERSION##v}_${os}-${arch}.tar.gz" sums.txt \
		| sha256sum -c

	tar zxf "hugo_${UTILITY_VERSION##v}_${os}-${arch}.tar.gz" \
		"hugo" \
		--transform="s/$/-${os}-${arch}/" \
		&& rm -rf "hugo_${UTILITY_VERSION##v}_${os}-${arch}.tar.gz"

	# strip binaries
	_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"
	# add our own shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"
done

rm -f sums.txt

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
