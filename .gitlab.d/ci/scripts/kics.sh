#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# kics.sh: compile kics for our specific architecures and store those as artifacts
# Usage: bash path/to/kics.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='kics'
UTILITY_GITHUB='Checkmarx/kics'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${KICS_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# we build them because we need arm64
git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
# shellcheck disable=SC2064 # cleanup, yes, expand now
trap "rm -rf $(pwd)/build" EXIT
cd build || exit
git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	export CGO_ENABLED=0
	export GOOS="${os}"
	export GOARCH="${arch}"
	export TARGET_BIN="${UTILITY_NAME}-${os}-${arch}"

	# hackity: arm64 build craps out: https://gitlab.com/yakshaving.art/hoardorr/-/jobs/2775191874
	# unless I have cycles to figure it out, plug it with a fake non-yet-supported mock
	if [[ "arm64" == "${arch}" ]]; then
		cat >| "${UTILITY_NAME}-${os}-${arch}" <<-EOF
			#!/bin/bash
			echo "${UTILITY_NAME} is not hoarded for arm64 yet"
			exit 42
		EOF
	else
		make build
		# try stripping
		_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"
	fi

	# add shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"

	# move to proper artifact location for CI to pick them up
	mv "${UTILITY_NAME}-${os}-${arch}" "${UTILITY_NAME}-${os}-${arch}.sha256" ../
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
