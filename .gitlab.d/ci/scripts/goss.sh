#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# goss.sh: compile goss for our specific architecures and store those as artifacts
# Usage: bash path/to/goss.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='goss'
UTILITY_GITHUB='goss-org/goss'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${GOSS_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# we build them because we need arm64
git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
# shellcheck disable=SC2064 # cleanup, yes, expand now
trap "rm -rf $(pwd)/build" EXIT
cd build || exit
git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
# shellcheck disable=2183,2086	# yes, expand variable please
sed -i "s/^build: .*/build: $(IFS=' '; printf "release\/goss-%s release\/goss-%s" ${MUCHOS_ARCHES//\//-})/" "Makefile"
export TRAVIS_TAG="${UTILITY_VERSION}"
# piggyback on what goss itself provides for simplicity
make build

# make artifacts available in the build dir
find "release/" \
	-type f \
	-name "${UTILITY_NAME}-*" \
	-exec mv {} .. \;

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
