#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# docker-slim.sh: compile docker-slim for our specific architecures and store those as artifacts
# Usage: bash path/to/docker-slim.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='slim'
UTILITY_GITHUB='slimtoolkit/slim'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${SLIM_TOOLKIT_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# we build them because we need arm64
git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
# shellcheck disable=SC2064 # cleanup, yes, expand now
trap "rm -rf $(pwd)/build" EXIT
cd build || exit
git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
# set common flags
LD_FLAGS="-s -w -X github.com/${UTILITY_GITHUB}/pkg/version.appVersionTag=$(git describe --tags) -X github.com/${UTILITY_GITHUB}/pkg/version.appVersionRev=$(git rev-parse HEAD) -X github.com/${UTILITY_GITHUB}/pkg/version.appVersionTime=$(date -u '+%Y-%m-%d_%I:%M:%S%p')"

go generate ./...
go generate "github.com/${UTILITY_GITHUB}/pkg/appbom"

# build for all the architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"
	for b in slim slim-sensor; do
		# build with a proper name
		CGO_ENABLED=0 GOOS="${os}" GOARCH="${arch}" go build \
			-ldflags="${LD_FLAGS}" \
			-trimpath \
			-mod vendor \
			-a -tags 'netgo osusergo' \
			-o "${b}-${os}-${arch}" \
			"./cmd/${b}"

		# strip binaries by default
		_multiarch_strip "${b}-${os}-${arch}" "${arch}"

		# add shasums
		sha256sum "${b}-${os}-${arch}" >| "${b}-${os}-${arch}.sha256"

		# move to proper artifact location for CI to pick them up
		mv "${b}-${os}-${arch}" "${b}-${os}-${arch}.sha256" ../
	done
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
