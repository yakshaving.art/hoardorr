#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# gitea.sh: get gitea for our specific architecures and store those as artifacts
# Usage: bash path/to/gitea.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='gitea'
UTILITY_GITHUB='go-gitea/gitea'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${GITEA_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
# install for all the architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	# fetch
	curl -qsSL --remote-name-all \
		"https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz" \
		"https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz.asc" \
		"https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz.sha256"

	# TODO: asc verification, for now sha check only
	sha256sum -c < "${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz.sha256"

	# drop the version from name
	binary="${UTILITY_NAME}-${os}-${arch}"
	xzcat "${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz" >| "${binary}"

	# try stripping
	_multiarch_strip "${binary}" "${arch}"

	# add shasums
	sha256sum "${binary}" >| "${binary}.sha256"

	# cleanup
	rm -f \
		"${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz" \
		"${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz.asc" \
		"${UTILITY_NAME}-${UTILITY_VERSION##v}-${os}-${arch}.xz.sha256"
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
