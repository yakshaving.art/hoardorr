#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# docker-buildx.sh: compile docker-buildx for our specific architecures and store those as artifacts
# Usage: bash path/to/docker-buildx.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='docker-buildx'
UTILITY_GITHUB='docker/buildx'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${DOCKER_BUILDX_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific

# fetch checksums first
curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/checksums.txt" \
	> checksums.txt

# hoard for all the enabled architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/buildx-${UTILITY_VERSION}.${os}-${arch}" \
		>| "docker-buildx-${os}-${arch}"

	# NOTE: format is different from ours, so we adapt
	grep "${os}-${arch}$" checksums.txt \
		| sed "s/buildx-${UTILITY_VERSION}.${os}-${arch}/docker-buildx-${os}-${arch}/g" \
		| sha256sum -c

	# strip files to save space if possible
	_multiarch_strip "docker-buildx-${os}-${arch}" "${arch}"

	# add our own shasums
	sha256sum "docker-buildx-${os}-${arch}" >| "docker-buildx-${os}-${arch}.sha256"
done

rm -f checksums.txt

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
