#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# k3s.sh: compile k3s for our specific architecures and store those as artifacts
# Usage: bash path/to/k3s.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='k3s'
UTILITY_GITHUB='k3s-io/k3s'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${K3S_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific
# install for all the architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	# fetch shasums first
	curl -qsSL -o "sha256sum-${arch}.txt" "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/sha256sum-${arch}.txt"

	# fetch binaries and check their shasums
	binary="k3s-${arch}"
	if [[ "amd64" == "${arch}" ]]; then
		binary="k3s"
	fi
	curl -qsSL -o "${binary}" "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/${binary}"
	grep "${binary}$" "sha256sum-${arch}.txt" | sha256sum -c

	# try stripping
	_multiarch_strip "${binary}" "${arch}"

	# rename properly
	mv "${binary}" "k3s-${os}-${arch}"

	# add shasums
	sha256sum "k3s-${os}-${arch}" >| "k3s-${os}-${arch}.sha256"

	# cleanup
	rm -f "sha256sum-${arch}.txt"
done

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
