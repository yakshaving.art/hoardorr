#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# download shellcheck for our specific architecures and store those as artifacts
# Usage: bash path/to/shellcheck.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

function getit() {
	wget -qqO - "${SHELLCHECK_URL}" \
		| tar -Jxf - "shellcheck-${SHELLCHECK_VERSION}/shellcheck" \
		--strip-components=1 \
		--transform="s/$/-${SHELLCHECK_ID}/"

	_multiarch_strip "shellcheck-${SHELLCHECK_ID}" "${SHELLCHECK_ID//*-}"

	echo "${SHELLCHECK_SHA}  shellcheck-${SHELLCHECK_ID}" >| "shellcheck-${SHELLCHECK_ID}.sha256"
	sha256sum -c < "shellcheck-${SHELLCHECK_ID}.sha256"
}

# linux/amd64
SHELLCHECK_URL="https://github.com/koalaman/shellcheck/releases/download/${SHELLCHECK_VERSION}/shellcheck-${SHELLCHECK_VERSION}.linux.x86_64.tar.xz"
SHELLCHECK_SHA="f35ae15a4677945428bdfe61ccc297490d89dd1e544cc06317102637638c6deb"
SHELLCHECK_ID="linux-amd64"
getit

# linux/arm64
SHELLCHECK_URL="https://github.com/koalaman/shellcheck/releases/download/${SHELLCHECK_VERSION}/shellcheck-${SHELLCHECK_VERSION}.linux.aarch64.tar.xz"
SHELLCHECK_SHA="4111c09318d10b93653a42179381273f31061b34987978346fbd19a6e81a74c3"
SHELLCHECK_ID="linux-arm64"
getit
