#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# doctl.sh: compile doctl for our specific architecures and store those as artifacts
# Usage: bash path/to/doctl.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='doctl'
UTILITY_GITHUB='digitalocean/doctl'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${DOCTL_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"
_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific

# fetch checksums first
curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/doctl-${UTILITY_VERSION##v}-checksums.sha256" > sums.txt

# hoard for all the enabled architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/doctl-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz" \
		> "doctl-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz"

	grep "${os}-${arch}.tar.gz" sums.txt \
		| sha256sum -c

	tar zxf "doctl-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz" "doctl" \
		--transform="s/$/-${os}-${arch}/" \
		&& rm -rf "doctl-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz"

	# strip
	_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"

	# add our own shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"
done

rm -f sums.txt

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
