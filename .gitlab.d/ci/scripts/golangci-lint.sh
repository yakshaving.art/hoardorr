#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# golangci-lint.sh: compile golangci-lint for our specific architecures and store those as artifacts
# Usage: bash path/to/golangci-lint.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

UTILITY_NAME='golangci-lint'
UTILITY_GITHUB='golangci/golangci-lint'
VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

# Get latest release, and bail out if we already hoarded it
UTILITY_VERSION="${GOLANGCI_LINT_VERSION:-$(_get_latest_version_from_github "${UTILITY_GITHUB}")}"

# WIP of a "force-rebuild" logic, for when we need to recompile and there's no new release upstream
FORCE_REBUILD="${1:-unset}"
function _force_rebuild() {

	git clone "https://github.com/${UTILITY_GITHUB}.git" 'build'
	# shellcheck disable=SC2064 # cleanup, yes, expand now
	trap "rm -rf $(pwd)/build" EXIT
	cd build || exit
	git -c advice.detachedHead=false checkout "${UTILITY_VERSION}"

	# shellcheck disable=SC2128
	IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
	for os_arch in "${MUCHOS_ARCHES[@]}"; do
		os="${os_arch%%/*}"
		arch="${os_arch##*/}"
		>&2 echo "building ${UTILITY_NAME} for ${os}/${arch}"
		CGO_ENABLED=0 GOOS="${os}" GOARCH="${arch}" go build -a -ldflags '-extldflags "-static"' -o . "./cmd/${UTILITY_NAME}"

		_multiarch_strip "${UTILITY_NAME}" "${arch}"
		# rename properly
		mv "${UTILITY_NAME}" "${UTILITY_NAME}-${os}-${arch}"

		# add shasums
		sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"

		# move to proper artifact location for CI to pick them up
		mv "${UTILITY_NAME}-${os}-${arch}" "${UTILITY_NAME}-${os}-${arch}.sha256" ../
	done

	# store installed version separately
	echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"

	exit 0	# for now, this is self-contained
}

if [[ "force-rebuild" == "${FORCE_REBUILD}" ]]; then
	_force_rebuild
fi
# end of WIP block


_skip_if_already_latest "${UTILITY_NAME}" "${UTILITY_VERSION}"

# build for all the architectures
# this part is tooling specific

# fetch checksums first
curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/golangci-lint-${UTILITY_VERSION##v}-checksums.txt" > sums.txt

# hoard for all the enabled architectures
# shellcheck disable=SC2128
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"

	curl -qsSL "https://github.com/${UTILITY_GITHUB}/releases/download/${UTILITY_VERSION}/golangci-lint-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz" \
		> "golangci-lint-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz"

	grep "${os}-${arch}.tar.gz" sums.txt \
		| sha256sum -c

	tar zxf "golangci-lint-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz" \
		"golangci-lint-${UTILITY_VERSION##v}-${os}-${arch}/golangci-lint" \
		--strip-components=1 \
		--transform="s/$/-${os}-${arch}/" \
		&& rm -rf "golangci-lint-${UTILITY_VERSION##v}-${os}-${arch}.tar.gz"

	# strip
	_multiarch_strip "${UTILITY_NAME}-${os}-${arch}" "${arch}"
	# add our own shasums
	sha256sum "${UTILITY_NAME}-${os}-${arch}" >| "${UTILITY_NAME}-${os}-${arch}.sha256"
done

rm -f sums.txt

# store installed version separately
echo -n "${UTILITY_VERSION}" >| "${VERSION_FILE}"
