#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# hashicorp.sh: install hashicorp tools
# Usage: bash path/to/hashicorp.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

# UTILITY_NAME='age'
# UTILITY_GITHUB=todo-refactor
# VERSION_FILE="$(pwd)/${UTILITY_NAME}-version-hoarded"

while read -r; do
	>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}'"; exit 42; }
done <<-EOF
	gpg
	parallel
	sha256sum
	unzip
	wget
EOF

# shellcheck disable=SC1091	# do not check it twice, it is checked by itself
source "$(realpath "$(dirname "${BASH_SOURCE[0]}")")/_functions.sh"

function getit() {
	local tool="${1:-unset}"
	local version="${2:-unset}"

	if [[ "unset" == "${tool}" || "unset" == "${version}" || "$#" != 2 ]]; then
		echo "usage: getit tool version"
		exit 42
	fi

	# determine latest version if it is explicitly set to "latest"
	if [[ "latest" == "${version}" ]]; then
		# NOTE: since there's an easy way to do it from GH, we use that instead of
		# wgetting and parsing HTML from releases.hashicorp.com. Also, we need to strip the "v".

		version="$(_get_latest_version_from_github "hashicorp/${tool}")"
		version="${version#v}"

		_skip_if_already_latest "${tool}" "${version}"
	fi

	local hashicorp_key="0x34365D9472D7468F"		# see https://www.hashicorp.com/security.html
	local hashicorp_url="https://releases.hashicorp.com"
	local keyservers=("keyserver.ubuntu.com" "keys.gnupg.net" "pgp.mit.edu" "pool.sks-keyservers.net" "subkeys.pgp.net")

	tmp="$(mktemp -d)"

	# fetch gpg key into tmp dir
	local key_received="false"
	for ksrv in "${keyservers[@]}"; do
		if gpg --quiet --homedir "${tmp}" --keyserver "${ksrv}" --recv-keys "${hashicorp_key}"; then
			key_received="true"
			break
		fi
	done
	if [[ "true" != "${key_received}" ]]; then
		>&2 echo "error: can't receive any keys"
		return 255
	fi

	# fetch all shasums
	if ! wget -qqP "${tmp}" \
			"${hashicorp_url}/${tool}/${version}/${tool}_${version}_SHA256SUMS" \
			"${hashicorp_url}/${tool}/${version}/${tool}_${version}_SHA256SUMS.sig"; then
		>&2 echo "error: can't download shasums from ${hashicorp_url} for '${tool}/${version}'!"
		return 255
	fi

	# check signatures
	if ! gpg --quiet --homedir "${tmp}" --verify "${tmp}/${tool}_${version}_SHA256SUMS.sig"; then
		>&2 echo "error: can't verify gpg signatures"
		return 255
	fi

	# process binaries for each arch
	IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
	for os_arch in "${MUCHOS_ARCHES[@]}"; do
		os="${os_arch%%/*}"
		arch="${os_arch##*/}"

		# fetch zip archives for required arches
		if ! wget -qqP "${tmp}" "${hashicorp_url}/${tool}/${version}/${tool}_${version}_${os}_${arch}.zip"; then
			>&2 echo "error: can't download zips from ${hashicorp_url} for '${tool}/${version}/${os}/${arch}'!"
			return 255
		fi

		# check shasums (sed to normalize paths)
		if ! grep "_${os}_${arch}.zip$" "${tmp}/${tool}_${version}_SHA256SUMS" | sed "s|  |  ${tmp}/|" | sha256sum -c >/dev/null; then
			>&2 echo "error: can't verify shasums for '${tool}/${os}/${arch}'"
			return 255
		fi

		# finally, unpack (and try to strip)
		unzip -p "${tmp}/${tool}_${version}_${os}_${arch}.zip" "${tool}" >| "${tool}-${os}-${arch}"
		_multiarch_strip "${tool}-${os}-${arch}" "${arch}"

		# generate hoardorr shasums
		sha256sum "${tool}-${os}-${arch}" >| "${tool}-${os}-${arch}.sha256"

		echo "	Hoarded hashicorp's '${tool}' tool, version '${version}', for os/arch '${os}/${arch}'"
	done

	# cleanup, but don't fail if can't remove the socket file
	# see the https://stackoverflow.com/a/45865627 for alternative solution,
	# which is not suitable for us, as we're not going to pkill gpg-agent if
	# this wrapper is run locally, this is just impolite
	rm -rf "${tmp}" || true

	# store installed version separately
	VERSION_FILE="${tool}-version-hoarded"
	echo -n "${version}" >| "${VERSION_FILE}"
}
export -f getit

# hoard all the tools with version defined
env \
	| gawk -F '[_=]' '/^HASHICORP_.*_VERSION=/ {printf "getit %s %s\n", tolower($2), $4}' \
	| sort \
	| SHELL="/bin/bash" parallel --no-notice
