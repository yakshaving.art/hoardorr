#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# a little bit of DRY

function _multiarch_strip() {
	# wrapper that knows the correct strip binary depending on the arch
	# if not on gitlab CI, then does nothing except for amd64
	# (not much sense in stripping non-native arches locally)
	# $1 : filename to strip
	# $2 : arch
	case "${2:-unset}" in
		amd64)
			strip "${1}"
			;;
		arm64)
			if [[ "true" != "${GITLAB_CI:-unset}" ]]; then
				>&2 echo "Not on gitlab CI, not stripping arm64 arch"
				return
			fi

			/usr/aarch64-none-elf/bin/strip "${1}"
			;;
		*)
			>&2 echo "Unsupported arch '${2}', please figure it out and add it here, fock'n failing for now"
			exit 42
			;;
	esac
}
export -f _multiarch_strip

function _get_latest_version_from_github() {
	# wrapper that gets project URL and echoes back the latest release from github
	# $1 : github project url, namespace/project format

	# Stolen from the internets
	# Username may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen
	# Project name may contain any valid alphanumeric including underscores
	if [[ ! ${1} =~ ^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]/[a-zA-Z0-9_-]+$ ]]; then
		>&2 echo "Error! Got invalid github repo name: '${1}', exiting!"
		exit 42
	fi

	while read -r; do
		>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}' or use the image that has it"; exit 42; }
	done <<-EOF
		curl
		jq
	EOF

	curl --silent "https://api.github.com/repos/${1}/releases" \
		| jq -r '[.[] | select(.prerelease==false and .draft==false)][0].tag_name'

	# TODO: safenet?
}
export -f _get_latest_version_from_github

function _skip_if_already_latest() {
	# wrapper to skip the build if we already have the latest version hoarded
	# NOTE: we download all prevously hoarded binaries/sums before any utility hoard
	# $1 : utility name, like 'age'
	# $2 : latest version

	local hoarded_version="dont-have-anything-yet-boss"
	if [[ -f "${1}-version-hoarded" ]]; then
		hoarded_version="$(cat "${1}-version-hoarded")"
	fi

	if [[ "${hoarded_version}" == "${2}" ]]; then
		>&2 echo "	Already hoarded tool '${1}' of latest version '${2}' in the past, skipping re-build"
		exit 0
	else
		>&2 echo "	Previously hoarded '${1}' of version '${hoarded_version}', but latest is '${2}'. Re-hoarding."
	fi
}
export -f _skip_if_already_latest
